package main

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/lhsanghcmus/go_upload_file/api"
	"net/http"
)

func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.POST("/file/upload",api.UploadFile)
	e.POST("/folder/create",api.CreateFolder)
	e.DELETE("/delete",api.Delete)
	e.Logger.Fatal(e.Start(":5050"))
}