package api

import (
	"cloud.google.com/go/storage"
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/lhsanghcmus/go_upload_file/model"
	"google.golang.org/api/option"
	"google.golang.org/appengine"

	"net/http"

)

func Delete(c echo.Context) error {
	var err error

	body := new(CreateFolderBody)
	if err := c.Bind(body); err != nil {
		return err
	}

	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}

	bucketName := "sanglh_example_bucket"  //your bucket name
	ctx := appengine.NewContext(c.Request())

	storageClient, err := storage.NewClient(ctx, option.WithCredentialsFile("key3.json"))
	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}


	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}


	sw := storageClient.Bucket(bucketName).Object(body.Directory)

	if err := sw.Delete(ctx); err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, &model.APIResponse{
		Status:  "OK",
		Data: fmt.Sprintf("Delete %s successfully",body.Directory),
	})
}
