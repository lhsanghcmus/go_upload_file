package api

import (
	"cloud.google.com/go/storage"
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/lhsanghcmus/go_upload_file/model"
	"google.golang.org/api/option"
	"google.golang.org/appengine"
	"io"
	"net/http"
	"net/url"
)



func UploadFile(c echo.Context) error {
	var err error

	f,uploadedFile,err:= c.Request().FormFile("file")
	directory:=c.Request().FormValue("directory")
	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}
	defer f.Close()

	bucketName := "sanglh_example_bucket"  //your bucket name
	ctx := appengine.NewContext(c.Request())

	storageClient, err := storage.NewClient(ctx, option.WithCredentialsFile("key3.json"))
	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}


	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}

	var fullPath string
	if directory == "" {
		fullPath = uploadedFile.Filename
	} else {
		fullPath = fmt.Sprintf("%s/%s",directory,uploadedFile.Filename)
	}

	sw := storageClient.Bucket(bucketName).Object(fullPath).NewWriter(ctx)
	if _,err:=io.Copy(sw,f);err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}

	if err := sw.Close(); err != nil {
		return err
	}

	u, err := url.Parse("/" + bucketName + "/" + sw.ObjectAttrs.Name)
	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, &model.APIResponse{
		Status:  "OK",
		Data: u.EscapedPath(),
	})

}

type CreateFolderBody struct {
	Directory string `json:"directory"`
}

func CreateFolder(c echo.Context) error {
	var err error

	body := new(CreateFolderBody)
	if err := c.Bind(body); err != nil {
		return err
	}

	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}

	bucketName := "sanglh_example_bucket"  //your bucket name
	ctx := appengine.NewContext(c.Request())

	storageClient, err := storage.NewClient(ctx, option.WithCredentialsFile("key3.json"))
	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}


	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}


	sw := storageClient.Bucket(bucketName).Object(body.Directory).NewWriter(ctx)

	if err := sw.Close(); err != nil {
		return err
	}

	u, err := url.Parse("/" + bucketName + "/" + sw.ObjectAttrs.Name)
	if err != nil {
		return c.JSON(http.StatusInternalServerError,&model.APIResponse{
			Status:  "ERROR",
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, &model.APIResponse{
		Status:  "OK",
		Data: u.EscapedPath(),
	})

}


