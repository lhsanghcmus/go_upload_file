module gitlab.com/lhsanghcmus/go_upload_file

go 1.15

require (
	cloud.google.com/go v0.79.0 // indirect
	cloud.google.com/go/storage v1.14.0
	github.com/labstack/echo/v4 v4.2.1
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20210309074719-68d13333faf2 // indirect
	google.golang.org/api v0.41.0
	google.golang.org/appengine v1.6.7
)
